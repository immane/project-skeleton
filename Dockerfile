FROM trafex/alpine-nginx-php7:1.9.0 as alpine

USER root
RUN apk add --no-cache php7-pdo php7-pdo_mysql php7-tokenizer php7-simplexml php7-zip php7-iconv

# Configure nginx
COPY .docker/config/nginx.conf /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY .docker/config/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
COPY .docker/config/php.ini /etc/php7/conf.d/custom.ini

# Configure supervisord
COPY .docker/config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R nobody.nobody /var/www && \
  chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/log/nginx

# Switch to use a non-root user from here on
USER nobody

# Add application
RUN rm -rf /var/www/*
COPY --chown=nobody . /var/www/
RUN rm -rf /var/www/var/*
RUN chmod -R 777 /var/www/var

WORKDIR /var/www

# Symfony command
#RUN php bin/console doctrine:schema:update --force
RUN php bin/console assets:install

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping
