Symfony 3.4 Project Skeleton
============================

A Symfony project created on Mar 14, 2019, 4:38 pm.


How to install
==============
    mkdir var
    composer install
    php bin/console doctrine:database:create
    php bin/console doctrine:schema:update --force
    php bin/console fos:user:create admin dummy@email.com P@ssw0rd --super-admin
    php bin/console cache:clear
