<?php
declare(strict_types=1);

namespace CommonBundle\DependencyInjection;

use EasyWeChat\Factory;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class WechatService
{
    /** @var ContainerInterface */
    protected $container;
    /** @var \Doctrine\ORM\EntityManager|object */
    protected $em;
    /** @var array */
    protected $official;
    /** @var array */
    protected $payment;

    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');

        $this->official = $container->getParameter('wechat.official');
        $this->miniprogram = $container->getParameter('wechat.miniprogram');
        $this->payment = $container->getParameter('wechat.payment');
    }

    public function official()
    {
        return Factory::officialAccount($this->official);
    }

    public function payment()
    {
        return Factory::payment($this->payment);
    }

    public function miniProgram()
    {
        return Factory::miniProgram($this->miniprogram);
    }
}