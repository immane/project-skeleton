<?php
declare(strict_types=1);

namespace CommonBundle\DependencyInjection;

use EasyWeChat\Factory;
use Omnipay\Alipay\AopPageGateway;
use Omnipay\Omnipay;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class AlipayService
{
    /** @var ContainerInterface */
    protected $container;
    /** @var \Doctrine\ORM\EntityManager|object */
    protected $em;
    /** @var array */
    protected $config;

    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->config = $container->getParameter('alibaba.pay');
    }

    public function createDefaultGateway()
    {
        /** @var AopPageGateway $gateway */
        $gateway = Omnipay::create('Alipay_AopPage');

        $gateway->setSignType($this->config['sign_type']); //RSA/RSA2
        $gateway->setAppId($this->config['app_id']);
        $gateway->setPrivateKey($this->config['private_key']);
        $gateway->setAlipayPublicKey($this->config['public_key']);

        return $gateway;
    }
}