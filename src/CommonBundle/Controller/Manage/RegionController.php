<?php

namespace CommonBundle\Controller\Manage;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\RegionService;
use CommonBundle\View\ApiView;
use CommonBundle\View\DeleteApiViewMixin;
use CommonBundle\View\DetailApiViewMixin;
use CommonBundle\View\ListApiViewMixin;
use CommonBundle\View\UpdateApiViewMixin;
use CommonBundle\View\CreateApiViewMixin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("/manage/regions", name="manage-region-")
 */
class RegionController extends RestController
{
    use ApiView, DetailApiViewMixin, ListApiViewMixin,
        CreateApiViewMixin, UpdateApiViewMixin, DeleteApiViewMixin;

    public function __construct() {
        $this->serviceClass = RegionService::class;
    }
}