<?php

namespace CommonBundle\Controller\Manage;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\UserService;
use CommonBundle\View\ApiView;
use CommonBundle\View\DeleteApiViewMixin;
use CommonBundle\View\DetailApiViewMixin;
use CommonBundle\View\ListApiViewMixin;
use CommonBundle\View\UpdateApiViewMixin;
use CommonBundle\View\CreateApiViewMixin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("/manage/users", name="manage-user-")
 */
class UserController extends RestController
{
    use ApiView, DetailApiViewMixin, ListApiViewMixin,
        CreateApiViewMixin, UpdateApiViewMixin, DeleteApiViewMixin;

    public function __construct() {
        $this->serviceClass = UserService::class;
    }
}