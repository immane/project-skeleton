<?php

namespace CommonBundle\Controller\Manage;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\UserProfileService;
use CommonBundle\View\ApiView;
use CommonBundle\View\DeleteApiViewMixin;
use CommonBundle\View\DetailApiViewMixin;
use CommonBundle\View\ListApiViewMixin;
use CommonBundle\View\UpdateApiViewMixin;
use CommonBundle\View\CreateApiViewMixin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("/manage/user-profiles", name="manage-user-profile-")
 */
class UserProfileController extends RestController
{
    use ApiView, DetailApiViewMixin, ListApiViewMixin,
        CreateApiViewMixin, UpdateApiViewMixin, DeleteApiViewMixin;

    public function __construct() {
        $this->serviceClass = UserProfileService::class;
    }

    protected function processCreateContent(array $content): array
    {
        if(array_key_exists('nickname', $content)) {
            $content['nickname'] = $this->removeEmoji($content['nickname']);
        }
        return $content;
    }

    protected function processUpdateContent(array $content): array
    {
        return $this->processCreateContent($content);
    }

    private function removeEmoji($str): string
    {
        $mbLen = mb_strlen($str);

        $strArr = [];
        for ($i = 0; $i < $mbLen; $i++) {
            $mbSubstr = mb_substr($str, $i, 1, 'utf-8');
            if (strlen($mbSubstr) >= 4) {
                continue;
            }
            $strArr[] = $mbSubstr;
        }

        return implode('', $strArr);
    }
}