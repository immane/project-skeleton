<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\SmsService;
use CommonBundle\Service\VerifyCodeService;
use CommonBundle\Utils\UUID;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class GeneralController extends RestController
{
    /**
     * Export all routes
     *
     * @SWG\Response(
     *     response=200,
     *     description="Export all routes",
     * )
     * @SWG\Parameter(name="page", in="query", type="string")
     * @SWG\Parameter(name="limit", in="query", type="string")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function routersListAction()
    {
        $routerService = $this->get('router');
        $routes = $routerService->getRouteCollection()->all();

        /** @var \Symfony\Component\Routing\Route $route */
        $routes = array_filter($routes, function($route, $key) {
            $apiPrefix = "api-";
            return strpos($key, $apiPrefix) === 0;
        }, ARRAY_FILTER_USE_BOTH);

        return $this->Success($routes);
    }

    /**
     * Generate and send verify code
     *
     * @Route("/generate-and-send-verify-code/{mobile}", methods={"GET"}, name="api-generate-and-send-verify-code")
     * @SWG\Response(
     *     response=200,
     *     description="Generate and send verify code",
     * )
     * @param $mobile
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \AlibabaCloud\Client\Exception\ClientException
     */
    public function generateAndSendVerifyCodeAction($mobile)
    {
        $verifyCodeService = $this->get(VerifyCodeService::class);
        $smsService = $this->get(SmsService::class);

        // generate verification code
        $code = $verifyCodeService->generateCode($mobile);

        if($code) {
            // send code via ali cloud sms.
            $result = $smsService->sendCode('86'.$mobile, $code);
            if($result) {
                return $this->Success('Send verification code success.');
            }
            else {
                return $this->Warning('Send verification code failure.');
            }
        }
        else {
            return $this->Warning('Error while generating verification code!');
        }
    }

    /**
     * @Route("/upload", methods={"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="Upload files",
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function uploadFilesAction(Request $request)
    {
        try {
            $filenames = [];

            /** @var File $file */
            foreach ($request->files as $file) {
                $filename = sprintf("%s.%s", UUID::v4(), $file->guessExtension());

                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('images_directory'),
                        $filename
                    );
                    $filenames[] = $filename;
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
            return $this->Success($filenames);
        }
        catch (\Exception $exception) {
            return $this->Warning($exception->getMessage(), $exception->getCode());
        }
    }
}
