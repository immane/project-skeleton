<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\OptionService;
use CommonBundle\View\ApiView;
use CommonBundle\View\DetailApiViewMixin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api/options", name="api-option-")
 */
class OptionController extends RestController
{
    use ApiView, DetailApiViewMixin;

    public function __construct() {
        $this->serviceClass = OptionService::class;
    }

    /**
     * @Route("/{key}", name="detail-by-key", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Option detail by key",
     * )
     * @param $key
     * @return Response
     */
    public function detailByKeyAction($key): Response
    {
        $service = $this->get($this->serviceClass);
        $filter = ['key' => $key];
        $filter = array_merge($filter, $this->commonFilter());

        $entity = $service->get($filter);

        return $entity ?
            $this->Success($entity):
            $this->Warning('Entity is not found');
    }
}
