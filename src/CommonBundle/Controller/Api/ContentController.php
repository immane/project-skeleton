<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\ContentService;
use CommonBundle\View\ApiView;
use CommonBundle\View\DetailApiViewMixin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api/contents", name="api-content-")
 */
class ContentController extends RestController
{
    use ApiView, DetailApiViewMixin;

    public function __construct() {
        $this->serviceClass = ContentService::class;
    }
}
