<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\CategoryService;
use CommonBundle\View\ApiView;
use CommonBundle\View\DetailApiViewMixin;
use CommonBundle\View\ListApiViewMixin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api/categories", name="api-category-")
 */
class CategoryController extends RestController
{
    use ApiView, ListApiViewMixin, DetailApiViewMixin;

    public function __construct() {
        $this->serviceClass = CategoryService::class;
    }
}
