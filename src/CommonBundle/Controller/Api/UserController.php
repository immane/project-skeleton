<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\UserService;
use CommonBundle\View\ApiView;
use CommonBundle\View\SingleCreateAndUpdateApiViewMixin;
use CommonBundle\View\SingleDetailApiViewMixin;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/api/user", name="api-user-")
 */
class UserController extends RestController
{
    use ApiView, SingleDetailApiViewMixin, SingleCreateAndUpdateApiViewMixin;

    public function __construct()
    {
        $this->serviceClass = UserService::class;
    }

    public function commonFilter()
    {
        return ['id' => $this->getUser()];
    }

    protected function defaultCreateValues(): array
    {
        throw new AccessDeniedException();
    }

    /**
     * @Route("/get-by-phone/{phone}", name="get-by-phone", methods={"GET"}, requirements={"phone"="1\d{10}"})
     * @SWG\Response(
     *     response=200,
     *     description="Get user id by phone",
     * )
     * @param $phone
     * @return Response
     */
    public function getByPhone($phone): Response
    {
        $service = $this->get($this->serviceClass);
        $entity = $service->get(['username' => $phone]);

        return $entity ?
            $this->Success($entity->getId()):
            $this->Warning('Entity is not found');
    }

}
