<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\AlbumService;
use CommonBundle\Service\BalanceService;
use CommonBundle\Service\ContentService;
use CommonBundle\Service\RegionService;
use CommonBundle\View\ApiView;
use CommonBundle\View\DetailApiViewMixin;
use CommonBundle\View\ListApiViewMixin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api/albums", name="api-album-")
 */
class AlbumController extends RestController
{
    use ApiView, DetailApiViewMixin;

    public function __construct() {
        $this->serviceClass = AlbumService::class;
    }

    /**
     * @Route("/by-title/{title}", name="detail-by-title", methods={"GET"})
     * @param $title
     * @return Response
     */
    public function detailByTitleAction($title): Response
    {
        $service = $this->get($this->serviceClass);
        $filter = ['title' => $title];
        $filter = array_merge($filter, $this->commonFilter());

        $entity = $service->get($filter);

        return $entity ?
            $this->Success($entity):
            $this->Warning('Entity is not found');
    }
}
