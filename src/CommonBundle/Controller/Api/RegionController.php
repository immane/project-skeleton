<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\BalanceService;
use CommonBundle\Service\RegionService;
use CommonBundle\View\ApiView;
use CommonBundle\View\ListApiViewMixin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api/regions", name="api-region-")
 */
class RegionController extends RestController
{
    use ApiView, ListApiViewMixin;

    public function __construct() {
        $this->serviceClass = RegionService::class;
    }
}
