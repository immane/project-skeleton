<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Service\UserProfileService;
use CommonBundle\View\ApiView;
use CommonBundle\View\SingleCreateAndUpdateApiViewMixin;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user-profile", name="api-user-profile-")
 */
class UserProfileController extends RestController
{
    use ApiView, SingleCreateAndUpdateApiViewMixin;

    public function __construct()
    {
        $this->serviceClass = UserProfileService::class;
    }

    public function commonFilter()
    {
        return ['user' => $this->getUser()];
    }

    protected function defaultCreateValues(): array
    {
        return ['user' => $this->getUser()];
    }
}
