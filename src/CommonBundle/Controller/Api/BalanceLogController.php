<?php

namespace CommonBundle\Controller\Api;

use CommonBundle\Controller\RestController;
use CommonBundle\Entity\BalanceLog;
use CommonBundle\Service\BalanceLogService;
use CommonBundle\Service\BalanceService;
use CommonBundle\View\ApiView;
use CommonBundle\View\ListApiViewMixin;
use Doctrine\Common\Collections\Criteria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api/balance-logs", name="api-balance-log-")
 */
class BalanceLogController extends RestController
{
    use ApiView, ListApiViewMixin;

    public function __construct() {
        $this->serviceClass = BalanceLogService::class;
    }

    public function commonFilter() {
        return ['user' => $this->getUser()];
    }
}
