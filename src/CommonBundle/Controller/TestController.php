<?php

namespace CommonBundle\Controller;

use CommonBundle\Entity\User;
use CommonBundle\Exception\MessageErrorHttpException;
use CommonBundle\Exception\MessageSuccessHttpException;
use Doctrine\Common\Collections\Criteria;
use Endroid\QrCode\QrCode;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class TestController extends RestController
{
    /**
     * @Route("/test/criteria", name="test-criteria")
     */
    public function testCriteria()
    {
        $em = $this->getDoctrine()->getManager();
        $sr = $em->getRepository(User::class);

        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->gt('defaultPrice', 80))
            ->andWhere(Criteria::expr()->lt('defaultPrice', 150));

        $services = $sr->matching($criteria);
        foreach($services as $s) dump($s);

        die();
    }

    /**
     * @Route("/test/exception", name="test-exception")
     */
    public function testException()
    {
        // show error message and no redirect
        throw new MessageErrorHttpException('Nothing is here!');

        // show error message and redirect
        throw new MessageErrorHttpException('Nothing is here!', 'http://www.baidu.com');

        // show error message and no redirect
        throw new MessageSuccessHttpException('Something is here!');

        // show error message and no redirect
        throw new MessageSuccessHttpException('Something is here!', 'http://www.google.com');
    }

    /**
     * @Route("/test/qrcode", name="test-qrcode")
     */
    public function testQrCode() {
        $qrCode = new QrCode('Wooooo! What the hell of your ass hole!???');
        return new Response( $qrCode->writeString(), 200, [
            'Content-Type'     => 'image/png',
            'Content-Disposition' => 'inline; filename="qrcode.png"'
        ]);
    }
}