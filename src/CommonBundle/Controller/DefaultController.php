<?php

namespace CommonBundle\Controller;

use CommonBundle\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends RestController
{
    /**
     * @Route("/", name="home")
     */
	public function home()
	{
		throw new NotFoundHttpException('Sorry, Nothing is here :(');
	}
}
