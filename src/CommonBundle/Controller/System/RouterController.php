<?php

namespace CommonBundle\Controller\System;

use CommonBundle\Controller\RestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/system/router", name="system-router-")
 */
class RouterController extends RestController
{
    /**
     * Export all routes
     *
     * @Route("", name="list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Export all routes",
     * )
     * @SWG\Parameter(name="page", in="query", type="string")
     * @SWG\Parameter(name="limit", in="query", type="string")
     * @SWG\Tag(name="system")
     *
     * @return Response
     */
    public function listAction()
    {
        $routerService = $this->get('router');
        $routes = $routerService->getRouteCollection()->all();

        /** @var \Symfony\Component\Routing\Route $route */
        /*
        $routes = array_filter($routes, function($route, $key) {
            $apiPrefix = "manage-";
            return strpos($key, $apiPrefix) === 0;
        }, ARRAY_FILTER_USE_BOTH);
        */

        return $this->Success($routes);
    }
}
