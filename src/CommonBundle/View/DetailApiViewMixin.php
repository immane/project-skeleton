<?php

namespace CommonBundle\View;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

trait DetailApiViewMixin
{
    /**
     * Api detail view
     *
     * @Route("/{id}", name="detail", methods={"GET"}, requirements={"id"="\d+"})
     * @SWG\Response(
     *     response=200,
     *     description="Api detail view",
     * )
     * @SWG\Parameter(name="@expands", in="query", type="string")
     *
     * @param $id
     * @return Response
     */
    public function detailAction($id): Response
    {
        $service = $this->get($this->serviceClass);
        $filter = $this->mixIdToCommonFilter($id);
        $entity = $service->get($filter, false);

        return $entity ?
            $this->Success($entity):
            $this->Warning('Entity is not found');
    }
}
