<?php

namespace CommonBundle\View;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

trait ListApiViewMixin
{
    protected function listFilter($filter = null)
    {
        /** list filter for list entities */
        return $filter;
    }

    protected function listProcessor($entities)
    {
        /** list processor for list entities */
        return $entities;
    }

    /**
     * @Route("", name="list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Api list view",
     * )
     * @SWG\Parameter(name="page", in="query", type="string", required=false)
     * @SWG\Parameter(name="limit", in="query", type="string", required=false)
     * @SWG\Parameter(name="@order", in="query", type="string", required=false)
     * @SWG\Parameter(name="@dql", in="query", type="string", required=false)
     * @SWG\Parameter(name="@select", in="query", type="string", required=false)
     * @SWG\Parameter(name="@groupBy", in="query", type="string", required=false)
     * @SWG\Parameter(name="@hints", in="query", type="string", required=false)
     * @SWG\Parameter(name="@filter", in="query", type="string", required=false)
     * @SWG\Parameter(name="@sort", in="query", type="string", required=false)
     * @SWG\Parameter(name="@expands", in="query", type="string", required=false)
     * @SWG\Parameter(name="@display", in="query", type="string", required=false)
     * @SWG\Parameter(name="@showDQL", in="query", type="boolean", required=false)
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(): Response
    {
        $service = $this->get($this->serviceClass);
        $filter = $this->listFilter($this->commonFilter());
        $entities = $this->listProcessor(
            $service->list($filter, null, false)
        );
        return $this->Success($entities);
    }
}
