<?php

namespace CommonBundle\View;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

trait DeleteApiViewMixin
{
    protected function deletionFilter($filter = null)
    {
        /** list filter for list entities */
        return $filter;
    }

    /**
     * Api delete view
     *
     * @Route("/{id}", name="delete", methods={"DELETE"}, requirements={"id"="\d+"})
     * @SWG\Response(
     *     response=200,
     *     description="Api delete view",
     * )
     * @param $id
     * @return Response
     */
    public function deleteAction($id): Response
    {
        $service = $this->get($this->serviceClass);
        $filter = $this->mixIdToCommonFilter($id);
        $filter = $this->deletionFilter($filter);
        $entity = $service->get($filter, false);

        return $service->remove($entity) ?
            $this->Success() : $this->Warning();
    }
}
