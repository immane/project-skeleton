<?php

namespace CommonBundle\View;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

trait SingleDetailApiViewMixin
{
    /**
     * Api single detail view
     *
     * @Route("", name="detail", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Api single detail view",
     * )
     * @SWG\Parameter(name="@expands", in="query", type="string")
     *
     * @return Response
     */
    public function detailAction(): Response
    {
        $service = $this->get($this->serviceClass);
        $filter = $this->commonFilter();
        $entity = $service->get($filter, false);

        return $this->Success($entity);
    }
}
