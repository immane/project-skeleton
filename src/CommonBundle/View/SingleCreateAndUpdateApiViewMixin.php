<?php

namespace CommonBundle\View;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait SingleCreateAndUpdateApiViewMixin
{
    protected function defaultCreateValues(): array
    {
        /** Default values */
        return [];
    }

    protected function defaultUpdateValues(): array
    {
        /** Default values */
        return [];
    }

    /**
     * Api single create and update view
     *
     * @Route("", name="update", methods={"PUT"})
     * @SWG\Response(
     *     response=200,
     *     description="Api single create and update view",
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function updateAction(Request $request): Response
    {
        $service = $this->get($this->serviceClass);
        $content = json_decode($request->getContent(), true) ? : [];

        $filter = $this->commonFilter();
        $entity = $service->get($filter, false);

        if(empty($entity)) {
            $entity = $service->new();
            $content = array_merge($content, $this->defaultCreateValues());
        }
        else {
            $content = array_merge($content, $this->defaultUpdateValues());
        }

        if ($entity = $service->update($entity, $content)) {
            return $this->Success($entity);
        } else {
            return $this->Warning();
        }
    }
}
