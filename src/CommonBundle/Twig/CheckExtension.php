<?php
namespace CommonBundle\Twig;

use Doctrine\Common\Collections\Collection;

class CheckExtension extends \Twig_Extension
{
    public function getTests()
    {
        return [
            'instanceof' => new \Twig_SimpleTest('instanceof', array($this, 'isInstanceOf'))
        ];
    }

    /**
     * @param $var
     * @param $instance
     * @return bool
     */
    public function isInstanceof($var, $instance) {
        if($instance === 'Collection') return $var instanceof Collection;
        return  $var instanceof $instance;
    }

    public function getName()
    {
        return 'twig_check_extension';
    }
}
