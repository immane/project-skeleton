<?php
declare(strict_types=1);

namespace CommonBundle\Service;

use CommonBundle\Entity\UserProfile;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class UserProfileService extends BaseService
{
    /** @var ContainerInterface */
    protected $container;
    /** @var EntityManager|object */
    protected $em;
    /** @var ObjectRepository|EntityRepository */
    protected $rep;

    function __construct(ContainerInterface $container)
    {
        parent::__construct($container, UserProfile::class);
    }
}
