<?php

namespace CommonBundle\Service;

use CommonBundle\Entity\Option;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;


final class OptionService extends BaseService
{
    /** @var ContainerInterface */
    protected $container;
    /** @var \Doctrine\ORM\EntityManager|object */
    protected $em;
    /** @var \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository */
    protected $rep;

    function __construct(ContainerInterface $container)
    {
        parent::__construct($container, Option::class);
    }

    public function get($object, bool $disableRequest = false)
    {
        if(is_numeric($object)) {
            return parent::get([
                'id' => $object,
                'readable' => true,
            ]);
        }
        elseif(is_array($object)) {
            return parent::get($object);
        }
        else {
            return parent::get([
                'key' => $object,
                'readable' => true,
            ]);
        }
    }

    public function value($object, $defaultValue = null)
    {
        $option = $this->get($object);
        if(empty($option) || empty($option->getValue())) return $defaultValue;
        else return $option->getValue();
    }

    public function getValue(string $key, $defaultValue = null)
    {
        return $this->value($key, $defaultValue);
    }

    public function setValue(string $key, string $value)
    {
        $option = $this->get($key);
        if(empty($option)) {
            $option = new Option();
            $option->setKey($key);
            $option->setName($key);
            $option->setReadable(true);
        };

        $this->update($option, ['value' => $value]);
    }
}
