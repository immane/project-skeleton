function init() {
    let form = document.forms['store']
    let longitudeEdit = form.elements['store_longitude']
    let latitudeEdit = form.elements['store_latitude']
    var longitude = longitudeEdit.value
    var latitude = latitudeEdit.value

    if(longitude !== '' && latitude !== '') {
        var myLatlng = new qq.maps.LatLng(latitude, longitude);
    }
    else {
        var myLatlng = new qq.maps.LatLng(39.916527,116.397128);
    }
    var myOptions = {
        zoom: 13,
        center: myLatlng,
    }
    var map = new qq.maps.Map(document.getElementsByClassName("store_map")[0], myOptions);

    var searchService,map,markers = [];
    var latlngBounds = new qq.maps.LatLngBounds();
    var searchService = new qq.maps.SearchService({
        complete : function(results){
            var pois = results.detail.pois;
            for(var i = 0,l = pois.length;i < l; i++){
                var poi = pois[i];
                latlngBounds.extend(poi.latLng);
                var marker = new qq.maps.Marker({
                    map:map,
                    position: poi.latLng
                });
                marker.setTitle(i+1);
                markers.push(marker);
            }
            map.fitBounds(latlngBounds);
        }
    });

    qq.maps.event.addListener(map, 'click', function(e) {
        let longitudeEdit = form.elements['store_longitude']
        let latitudeEdit = form.elements['store_latitude']

        longitudeEdit.value = e.latLng.lng
        latitudeEdit.value = e.latLng.lat
    });

    let addressEdit = form.elements['store_address']
    addressEdit.onkeyup = function () {
        //console.log(this.value)
        searchService.setLocation('');
        searchService.search(this.value);
    }
}

function loadScript() {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "https://map.qq.com/api/js?v=2.exp&key=4ESBZ-6TO34-7TFUE-XPXBK-O2G26-OLFKZ&callback=init";
    document.body.appendChild(script);
}

window.onload = loadScript;

$(function() {
    $('#store_address').parent().append(
        $('<div>').addClass('store_map').css({'height': '500px'})
    );
});