<?php

// src/CommonBundle/Entity/Notification.php
 
namespace CommonBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="notification")
 * @ORM\Entity
 */
class Notification
{
    const STATUS_DISPATCHED = 0;
    const STATUS_FETCHED = 1;
    const STATUS_READ = 2;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\User")
     * @ORM\JoinColumn(name="from_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $from;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\User")
     * @ORM\JoinColumn(name="to_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $to;

    /**
     * @ORM\Column(type="string", length=4096)
     */
    protected $message;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * Notification status
     *  0: Not fetched, Unread
     *  1: Fetched, Unread
     *  2: Read
     * @ORM\Column(type="integer", length=5, options={"default":0})
     */
    protected $status;

    /**
     * @ORM\Column(name="created_time", type="datetime", nullable=false)
     */
    protected $createdTime;

    /**
     * @ORM\Column(name="modified_time", type="datetime", nullable=false)
     */
    protected $modifiedTime;

    public function __construct() {
        // we set up "created" + "modified"
        $this->setCreatedTime(new \DateTime());
        if ($this->getModifiedTime() == null) {
            $this->setModifiedTime(new \DateTime());
        }
    }

    public function __toString() {
        return sprintf("[#%d] %s", $this->getId(), $this->getMessage() );
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime() {
        // update the modified time
        $this->setModifiedTime(new \DateTime());
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message.
     *
     * @param string $message
     *
     * @return Notification
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message.
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Notification
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return Notification
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdTime.
     *
     * @param \DateTime $createdTime
     *
     * @return Notification
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime.
     *
     * @return \DateTime
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set modifiedTime.
     *
     * @param \DateTime $modifiedTime
     *
     * @return Notification
     */
    public function setModifiedTime($modifiedTime)
    {
        $this->modifiedTime = $modifiedTime;

        return $this;
    }

    /**
     * Get modifiedTime.
     *
     * @return \DateTime
     */
    public function getModifiedTime()
    {
        return $this->modifiedTime;
    }

    /**
     * Set from.
     *
     * @param \CommonBundle\Entity\User|null $from
     *
     * @return Notification
     */
    public function setFrom(\CommonBundle\Entity\User $from = null)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from.
     *
     * @return \CommonBundle\Entity\User|null
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to.
     *
     * @param \CommonBundle\Entity\User|null $to
     *
     * @return Notification
     */
    public function setTo(\CommonBundle\Entity\User $to = null)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to.
     *
     * @return \CommonBundle\Entity\User|null
     */
    public function getTo()
    {
        return $this->to;
    }
}
