<?php

// src/CommonBundle/Entity/UserProfile.php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="user_profile")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserProfile
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    // Common

    /**
     * @ORM\OneToOne(targetEntity="CommonBundle\Entity\User", inversedBy="profile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;


    // Wechat

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $nickname;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $avatarUrl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $province;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $language;


    // ID cards

    /**
     * @ORM\Column(type="boolean")
     */
    private $realnameVerification = false;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $realname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $idCardNumber;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $idCardFront;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $idCardRear;

    // System

    /**
     * @ORM\Column(name="created_time", type="datetime", nullable=false)
     */
    private $createdTime;

    /**
     * @ORM\Column(name="modified_time", type="datetime", nullable=false)
     */
    private $modifiedTime;

    public function __construct()
    {
        // we set up "created" + "modified"
        $this->setCreatedTime(new \DateTime());
        if ($this->getModifiedTime() == null) {
            $this->setModifiedTime(new \DateTime());
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateCommon()
    {
    }

    public function __toString()
    {
        if ($this->nickname) {
            return $this->nickname;
        } else {
            return $this->getUser()->getUsername();
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set summary.
     *
     * @param string|null $summary
     *
     * @return UserProfile
     */
    public function setSummary($summary = null)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary.
     *
     * @return string|null
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set nickname.
     *
     * @param string|null $nickname
     *
     * @return UserProfile
     */
    public function setNickname($nickname = null)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname.
     *
     * @return string|null
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set gender.
     *
     * @param string|null $gender
     *
     * @return UserProfile
     */
    public function setGender($gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return string|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set avatarUrl.
     *
     * @param string|null $avatarUrl
     *
     * @return UserProfile
     */
    public function setAvatarUrl($avatarUrl = null)
    {
        $this->avatarUrl = $avatarUrl;

        return $this;
    }

    /**
     * Get avatarUrl.
     *
     * @return string|null
     */
    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }

    /**
     * Set country.
     *
     * @param string|null $country
     *
     * @return UserProfile
     */
    public function setCountry($country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set province.
     *
     * @param string|null $province
     *
     * @return UserProfile
     */
    public function setProvince($province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province.
     *
     * @return string|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set city.
     *
     * @param string|null $city
     *
     * @return UserProfile
     */
    public function setCity($city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set language.
     *
     * @param string|null $language
     *
     * @return UserProfile
     */
    public function setLanguage($language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return string|null
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set realnameVerification.
     *
     * @param bool $realnameVerification
     *
     * @return UserProfile
     */
    public function setRealnameVerification($realnameVerification)
    {
        $this->realnameVerification = $realnameVerification;

        return $this;
    }

    /**
     * Get realnameVerification.
     *
     * @return bool
     */
    public function getRealnameVerification()
    {
        return $this->realnameVerification;
    }

    /**
     * Set realname.
     *
     * @param string|null $realname
     *
     * @return UserProfile
     */
    public function setRealname($realname = null)
    {
        $this->realname = $realname;

        return $this;
    }

    /**
     * Get realname.
     *
     * @return string|null
     */
    public function getRealname()
    {
        return $this->realname;
    }

    /**
     * Set idCardNumber.
     *
     * @param string|null $idCardNumber
     *
     * @return UserProfile
     */
    public function setIdCardNumber($idCardNumber = null)
    {
        $this->idCardNumber = $idCardNumber;

        return $this;
    }

    /**
     * Get idCardNumber.
     *
     * @return string|null
     */
    public function getIdCardNumber()
    {
        return $this->idCardNumber;
    }

    /**
     * Set idCardFront.
     *
     * @param string|null $idCardFront
     *
     * @return UserProfile
     */
    public function setIdCardFront($idCardFront = null)
    {
        $this->idCardFront = $idCardFront;

        return $this;
    }

    /**
     * Get idCardFront.
     *
     * @return string|null
     */
    public function getIdCardFront()
    {
        return $this->idCardFront;
    }

    /**
     * Set idCardRear.
     *
     * @param string|null $idCardRear
     *
     * @return UserProfile
     */
    public function setIdCardRear($idCardRear = null)
    {
        $this->idCardRear = $idCardRear;

        return $this;
    }

    /**
     * Get idCardRear.
     *
     * @return string|null
     */
    public function getIdCardRear()
    {
        return $this->idCardRear;
    }

    /**
     * Set createdTime.
     *
     * @param \DateTime $createdTime
     *
     * @return UserProfile
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime.
     *
     * @return \DateTime
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set modifiedTime.
     *
     * @param \DateTime $modifiedTime
     *
     * @return UserProfile
     */
    public function setModifiedTime($modifiedTime)
    {
        $this->modifiedTime = $modifiedTime;

        return $this;
    }

    /**
     * Get modifiedTime.
     *
     * @return \DateTime
     */
    public function getModifiedTime()
    {
        return $this->modifiedTime;
    }

    /**
     * Set user.
     *
     * @param \CommonBundle\Entity\User $user
     *
     * @return UserProfile
     */
    public function setUser(\CommonBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \CommonBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return UserProfile
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
