<?php

// src/CommonBundle/Entity/Region.php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"parent_id", "name"})
 * })
 * @ORM\Entity
 */
class Region
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Region")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\Region", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\Column(type="integer")
     */
    private $levelType = 0;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $shortName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $parentPath;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $province;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $district;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $provinceShortName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $cityShortName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $districtShortName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $provincePinyin;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $cityPinyin;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $districtPinyin;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $pinyin;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $jianpin;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstChar;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $cityCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $zipCode;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude = 0;

    /**
     * @ORM\Column(type="float")
     */
    private $latitude = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $remark1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $remark2;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set levelType.
     *
     * @param int $levelType
     *
     * @return Region
     */
    public function setLevelType($levelType)
    {
        $this->levelType = $levelType;

        return $this;
    }

    /**
     * Get levelType.
     *
     * @return int
     */
    public function getLevelType()
    {
        return $this->levelType;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Region
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortName.
     *
     * @param string|null $shortName
     *
     * @return Region
     */
    public function setShortName($shortName = null)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName.
     *
     * @return string|null
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set parentPath.
     *
     * @param string|null $parentPath
     *
     * @return Region
     */
    public function setParentPath($parentPath = null)
    {
        $this->parentPath = $parentPath;

        return $this;
    }

    /**
     * Get parentPath.
     *
     * @return string|null
     */
    public function getParentPath()
    {
        return $this->parentPath;
    }

    /**
     * Set province.
     *
     * @param string|null $province
     *
     * @return Region
     */
    public function setProvince($province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province.
     *
     * @return string|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set city.
     *
     * @param string|null $city
     *
     * @return Region
     */
    public function setCity($city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set district.
     *
     * @param string|null $district
     *
     * @return Region
     */
    public function setDistrict($district = null)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district.
     *
     * @return string|null
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set provinceShortName.
     *
     * @param string|null $provinceShortName
     *
     * @return Region
     */
    public function setProvinceShortName($provinceShortName = null)
    {
        $this->provinceShortName = $provinceShortName;

        return $this;
    }

    /**
     * Get provinceShortName.
     *
     * @return string|null
     */
    public function getProvinceShortName()
    {
        return $this->provinceShortName;
    }

    /**
     * Set cityShortName.
     *
     * @param string|null $cityShortName
     *
     * @return Region
     */
    public function setCityShortName($cityShortName = null)
    {
        $this->cityShortName = $cityShortName;

        return $this;
    }

    /**
     * Get cityShortName.
     *
     * @return string|null
     */
    public function getCityShortName()
    {
        return $this->cityShortName;
    }

    /**
     * Set districtShortName.
     *
     * @param string|null $districtShortName
     *
     * @return Region
     */
    public function setDistrictShortName($districtShortName = null)
    {
        $this->districtShortName = $districtShortName;

        return $this;
    }

    /**
     * Get districtShortName.
     *
     * @return string|null
     */
    public function getDistrictShortName()
    {
        return $this->districtShortName;
    }

    /**
     * Set provincePinyin.
     *
     * @param string|null $provincePinyin
     *
     * @return Region
     */
    public function setProvincePinyin($provincePinyin = null)
    {
        $this->provincePinyin = $provincePinyin;

        return $this;
    }

    /**
     * Get provincePinyin.
     *
     * @return string|null
     */
    public function getProvincePinyin()
    {
        return $this->provincePinyin;
    }

    /**
     * Set cityPinyin.
     *
     * @param string|null $cityPinyin
     *
     * @return Region
     */
    public function setCityPinyin($cityPinyin = null)
    {
        $this->cityPinyin = $cityPinyin;

        return $this;
    }

    /**
     * Get cityPinyin.
     *
     * @return string|null
     */
    public function getCityPinyin()
    {
        return $this->cityPinyin;
    }

    /**
     * Set districtPinyin.
     *
     * @param string|null $districtPinyin
     *
     * @return Region
     */
    public function setDistrictPinyin($districtPinyin = null)
    {
        $this->districtPinyin = $districtPinyin;

        return $this;
    }

    /**
     * Get districtPinyin.
     *
     * @return string|null
     */
    public function getDistrictPinyin()
    {
        return $this->districtPinyin;
    }

    /**
     * Set pinyin.
     *
     * @param string|null $pinyin
     *
     * @return Region
     */
    public function setPinyin($pinyin = null)
    {
        $this->pinyin = $pinyin;

        return $this;
    }

    /**
     * Get pinyin.
     *
     * @return string|null
     */
    public function getPinyin()
    {
        return $this->pinyin;
    }

    /**
     * Set jianpin.
     *
     * @param string|null $jianpin
     *
     * @return Region
     */
    public function setJianpin($jianpin = null)
    {
        $this->jianpin = $jianpin;

        return $this;
    }

    /**
     * Get jianpin.
     *
     * @return string|null
     */
    public function getJianpin()
    {
        return $this->jianpin;
    }

    /**
     * Set firstChar.
     *
     * @param string|null $firstChar
     *
     * @return Region
     */
    public function setFirstChar($firstChar = null)
    {
        $this->firstChar = $firstChar;

        return $this;
    }

    /**
     * Get firstChar.
     *
     * @return string|null
     */
    public function getFirstChar()
    {
        return $this->firstChar;
    }

    /**
     * Set cityCode.
     *
     * @param string|null $cityCode
     *
     * @return Region
     */
    public function setCityCode($cityCode = null)
    {
        $this->cityCode = $cityCode;

        return $this;
    }

    /**
     * Get cityCode.
     *
     * @return string|null
     */
    public function getCityCode()
    {
        return $this->cityCode;
    }

    /**
     * Set zipCode.
     *
     * @param string|null $zipCode
     *
     * @return Region
     */
    public function setZipCode($zipCode = null)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set longitude.
     *
     * @param float $longitude
     *
     * @return Region
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude.
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude.
     *
     * @param float $latitude
     *
     * @return Region
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude.
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set remark1.
     *
     * @param string|null $remark1
     *
     * @return Region
     */
    public function setRemark1($remark1 = null)
    {
        $this->remark1 = $remark1;

        return $this;
    }

    /**
     * Get remark1.
     *
     * @return string|null
     */
    public function getRemark1()
    {
        return $this->remark1;
    }

    /**
     * Set remark2.
     *
     * @param string|null $remark2
     *
     * @return Region
     */
    public function setRemark2($remark2 = null)
    {
        $this->remark2 = $remark2;

        return $this;
    }

    /**
     * Get remark2.
     *
     * @return string|null
     */
    public function getRemark2()
    {
        return $this->remark2;
    }

    /**
     * Set parent.
     *
     * @param \CommonBundle\Entity\Region|null $parent
     *
     * @return Region
     */
    public function setParent(\CommonBundle\Entity\Region $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \CommonBundle\Entity\Region|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child.
     *
     * @param \CommonBundle\Entity\Region $child
     *
     * @return Region
     */
    public function addChild(\CommonBundle\Entity\Region $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param \CommonBundle\Entity\Region $child
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChild(\CommonBundle\Entity\Region $child)
    {
        return $this->children->removeElement($child);
    }

    /**
     * Get children.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
}
