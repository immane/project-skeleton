<?php

// src/CommonBundle/Entity/Balance.php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="balance")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Balance
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="CommonBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default"="0"})
     */
    private $amount = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default"="0"})
     */
    private $point = 0;

    /**
     * @ORM\Column(name="last_modified_time", type="datetime", nullable=false)
     */
    private $lastModifiedTime;

    public function __construct()
    {
        // we set up "modified"
        if ($this->getLastModifiedTime() == null) {
            $this->setLastModifiedTime(new \DateTime());
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
        // update the modified time
        $this->setLastModifiedTime(new \DateTime());
    }

    public function __toString()
    {
        return $this->id;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount.
     *
     * @param string $amount
     *
     * @return Balance
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set point.
     *
     * @param string $point
     *
     * @return Balance
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point.
     *
     * @return string
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set lastModifiedTime.
     *
     * @param \DateTime $lastModifiedTime
     *
     * @return Balance
     */
    public function setLastModifiedTime($lastModifiedTime)
    {
        $this->lastModifiedTime = $lastModifiedTime;

        return $this;
    }

    /**
     * Get lastModifiedTime.
     *
     * @return \DateTime
     */
    public function getLastModifiedTime()
    {
        return $this->lastModifiedTime;
    }

    /**
     * Set user.
     *
     * @param \CommonBundle\Entity\User $user
     *
     * @return Balance
     */
    public function setUser(\CommonBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \CommonBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
