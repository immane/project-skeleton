<?php

// src/CommonBundle/Entity/Option.php
 
namespace CommonBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
 
/**
 * @ORM\Table(name="`option`")
 * @ORM\Entity
 */
class Option
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="`key`", type="string", length=64, unique=true)
     */
    private $key;

    /**
     * @ORM\Column(name="`name`", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="`value`", type="string", length=4096, nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="boolean")
     */
    private $readable;

    public function __toString()
    {
        return $this->key;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key.
     *
     * @param string $key
     *
     * @return Option
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Option
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value.
     *
     * @param string|null $value
     *
     * @return Option
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set readable.
     *
     * @param bool $readable
     *
     * @return Option
     */
    public function setReadable($readable)
    {
        $this->readable = $readable;

        return $this;
    }

    /**
     * Get readable.
     *
     * @return bool
     */
    public function getReadable()
    {
        return $this->readable;
    }
}
