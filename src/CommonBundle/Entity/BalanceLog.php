<?php

// src/CommonBundle/Entity/BalanceLog.php
 
namespace CommonBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
 
/**
 * @ORM\Table(name="balance_log")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class BalanceLog
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\User")
     * @ORM\JoinColumn(name="source_id", referencedColumnName="id")
     */
    private $source;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(name="`before`", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $before;

    /**
     * @ORM\Column(name="`after`", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $after;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $extraData;

    /**
     * @ORM\Column(name="created_time", type="datetime", nullable=false)
     */
    private $createdTime;

    public function __construct() {
        // we set up "created"
        $this->setCreatedTime(new \DateTime());
    }

    public function __toString() {
		return $this->id;
	}

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return BalanceLog
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount.
     *
     * @param string|null $amount
     *
     * @return BalanceLog
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return string|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set before.
     *
     * @param string|null $before
     *
     * @return BalanceLog
     */
    public function setBefore($before = null)
    {
        $this->before = $before;

        return $this;
    }

    /**
     * Get before.
     *
     * @return string|null
     */
    public function getBefore()
    {
        return $this->before;
    }

    /**
     * Set after.
     *
     * @param string|null $after
     *
     * @return BalanceLog
     */
    public function setAfter($after = null)
    {
        $this->after = $after;

        return $this;
    }

    /**
     * Get after.
     *
     * @return string|null
     */
    public function getAfter()
    {
        return $this->after;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return BalanceLog
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdTime.
     *
     * @param \DateTime $createdTime
     *
     * @return BalanceLog
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime.
     *
     * @return \DateTime
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set user.
     *
     * @param \CommonBundle\Entity\User|null $user
     *
     * @return BalanceLog
     */
    public function setUser(\CommonBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \CommonBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set source.
     *
     * @param \CommonBundle\Entity\User|null $source
     *
     * @return BalanceLog
     */
    public function setSource(\CommonBundle\Entity\User $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return \CommonBundle\Entity\User|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set extraData.
     *
     * @param json|null $extraData
     *
     * @return BalanceLog
     */
    public function setExtraData($extraData = null)
    {
        $this->extraData = $extraData;

        return $this;
    }

    /**
     * Get extraData.
     *
     * @return json|null
     */
    public function getExtraData()
    {
        return $this->extraData;
    }
}
