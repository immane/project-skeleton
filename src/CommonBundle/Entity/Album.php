<?php

// src/CommonBundle/Entity/Album.php
 
namespace CommonBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
 
/**
 * @ORM\Table(name="album")
 * @ORM\Entity
 */
class Album
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

	/**
     * @ORM\Column(type="string", length=32, nullable=true, options={"default"="default"})
     */
    private $type;

	/**
     * @ORM\ManyToMany(targetEntity="CommonBundle\Entity\Picture", cascade={"persist","remove"}, orphanRemoval=true)
	 * @ORM\JoinTable(
	 *		name="album_pictures",
     *      joinColumns={@ORM\JoinColumn(name="album_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="picture_id", referencedColumnName="id")}
     *  )
     */
    private $pictures;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    public function __construct() {
        $this->pictures = new \Doctrine\Common\Collections\ArrayCollection();
    }
	
	public function __toString() {
		return sprintf("%d: %s", $this->id, $this->title);
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Album
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Album
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add picture
     *
     * @param \CommonBundle\Entity\Picture $picture
     *
     * @return Album
     */
    public function addPicture(\CommonBundle\Entity\Picture $picture)
    {
        $this->pictures[] = $picture;

        return $this;
    }

    /**
     * Remove picture
     *
     * @param \CommonBundle\Entity\Picture $picture
     */
    public function removePicture(\CommonBundle\Entity\Picture $picture)
    {
        $this->pictures->removeElement($picture);
    }

    /**
     * Get pictures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Album
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
