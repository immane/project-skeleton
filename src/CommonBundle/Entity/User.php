<?php

namespace CommonBundle\Entity;
 
use CommonBundle\Utils\UUID;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
 
/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="CommonBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser implements AdvancedUserInterface, \Serializable
{
    const USER_LEVEL_DEFAULT = 0;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    protected $token;
 
    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="integer")
     */
    protected $level = self::USER_LEVEL_DEFAULT;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $wechatOpenId;

    /**
     * @ORM\Column(nullable=true, type="string", length=64, nullable=true)
     */
    protected $wechatUnionId;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Business")
     * @ORM\JoinColumn(name="business_id", referencedColumnName="id")
     */
    protected $business;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\User")
     * @ORM\JoinColumn(name="recommended_user_id", referencedColumnName="id")
     */
    protected $recommendedUser;

    /**
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\User", mappedBy="recommendedUser")
     */
    protected $recommendUsers;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdTime;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modifiedTime;

    /**
     * @ORM\OneToOne(targetEntity="CommonBundle\Entity\UserProfile", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $profile;


    public function __construct()
    {
        parent::__construct();

        // we set up "created" + "modified"
        $this->setCreatedTime(new \DateTime());
        if ($this->getModifiedTime() == null) {
            $this->setModifiedTime(new \DateTime());
        }

        // generate api key.
        $this->setToken(UUID::v4());

        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

	public function __toString() {
        if( $profile = $this->getProfile() ) {
            if($profile->getRealname())
                return $profile->getRealname();
            else
                return $profile->getNickname();
        }
		return $this->username;
	}

    public function getRecommendChain($level = 3)
    {
        $chain = [];
        $parent = $this->getRecommendedUser();
        while($level-- && $parent) {
            $chain[] = $parent->getId();
            $parent = $parent->getRecommendedUser();
        }

        return $chain;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
        // update the modified time
        $this->setModifiedTime(new \DateTime());
    }

    public function eraseCredentials() {}
 
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
			$this->enabled,
            // $this->salt,
        ));
    }
 
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
			$this->enabled,
            // $this->salt
        ) = unserialize($serialized);
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return User
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set profile.
     *
     * @param \CommonBundle\Entity\UserProfile|null $profile
     *
     * @return User
     */
    public function setProfile(\CommonBundle\Entity\UserProfile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \CommonBundle\Entity\UserProfile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set wechatOpenId.
     *
     * @param string|null $wechatOpenId
     *
     * @return User
     */
    public function setWechatOpenId($wechatOpenId = null)
    {
        $this->wechatOpenId = $wechatOpenId;

        return $this;
    }

    /**
     * Get wechatOpenId.
     *
     * @return string|null
     */
    public function getWechatOpenId()
    {
        return $this->wechatOpenId;
    }

    /**
     * Set wechatUnionId.
     *
     * @param string|null $wechatUnionId
     *
     * @return User
     */
    public function setWechatUnionId($wechatUnionId = null)
    {
        $this->wechatUnionId = $wechatUnionId;

        return $this;
    }

    /**
     * Get wechatUnionId.
     *
     * @return string|null
     */
    public function getWechatUnionId()
    {
        return $this->wechatUnionId;
    }

    /**
     * Set createdTime.
     *
     * @param \DateTime $createdTime
     *
     * @return User
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime.
     *
     * @return \DateTime
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set modifiedTime.
     *
     * @param \DateTime $modifiedTime
     *
     * @return User
     */
    public function setModifiedTime($modifiedTime)
    {
        $this->modifiedTime = $modifiedTime;

        return $this;
    }

    /**
     * Get modifiedTime.
     *
     * @return \DateTime
     */
    public function getModifiedTime()
    {
        return $this->modifiedTime;
    }

    /**
     * Set token.
     *
     * @param string $token
     *
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set level.
     *
     * @param int $level
     *
     * @return User
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level.
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set recommendedUser.
     *
     * @param \CommonBundle\Entity\User|null $recommendedUser
     *
     * @return User
     */
    public function setRecommendedUser(\CommonBundle\Entity\User $recommendedUser = null)
    {
        $this->recommendedUser = $recommendedUser;

        return $this;
    }

    /**
     * Get recommendedUser.
     *
     * @return \CommonBundle\Entity\User|null
     */
    public function getRecommendedUser()
    {
        return $this->recommendedUser;
    }

    /**
     * Add recommendUser.
     *
     * @param \CommonBundle\Entity\User $recommendUser
     *
     * @return User
     */
    public function addRecommendUser(\CommonBundle\Entity\User $recommendUser)
    {
        $this->recommendUsers[] = $recommendUser;

        return $this;
    }

    /**
     * Remove recommendUser.
     *
     * @param \CommonBundle\Entity\User $recommendUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRecommendUser(\CommonBundle\Entity\User $recommendUser)
    {
        return $this->recommendUsers->removeElement($recommendUser);
    }

    /**
     * Get recommendUsers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecommendUsers()
    {
        return $this->recommendUsers;
    }

    /**
     * Set business.
     *
     * @param \CommonBundle\Entity\Business|null $business
     *
     * @return User
     */
    public function setBusiness(\CommonBundle\Entity\Business $business = null)
    {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business.
     *
     * @return \CommonBundle\Entity\Business|null
     */
    public function getBusiness()
    {
        return $this->business;
    }
}
